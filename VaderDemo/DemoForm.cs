﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using VaderDemo.Twitter;

namespace VaderDemo
{
    public partial class DemoForm : Form
    {
        private IVaderDevice vaderDevice;
        private ITweetQueue tweetQueue;

        string twitterSearchText;

        public DemoForm()
        {
            InitializeComponent();

            string serialPortName = ConfigurationManager.AppSettings["SerialPort"];
            string twitterConsumerKey = ConfigurationManager.AppSettings["TwitterConsumerKey"];
            string twitterConsumerSecret = ConfigurationManager.AppSettings["TwitterConsumerSecret"];
            twitterSearchText = ConfigurationManager.AppSettings["TwitterSearch"];

            // Sorry - no time for dependency injection
            vaderDevice = new VaderDevice(serialPortName);
            //vaderDevice = new DebugVaderDevice();
            tweetQueue = new TweetQueue(new TweetClient(twitterConsumerKey, twitterConsumerSecret), twitterSearchText);

            userLabel.Text = tweetTextBox.Text = commandLabel.Text = String.Empty;
            
        }

        private void redButton_Click(object sender, EventArgs e)
        {
            vaderDevice.SetColour(Color.Red);
        }

        private void greenButton_Click(object sender, EventArgs e)
        {
            vaderDevice.SetColour(Color.FromArgb(0, 255, 0));
        }

        private void blueButton_Click(object sender, EventArgs e)
        {
            vaderDevice.SetColour(Color.Blue);
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog.ShowDialog();
            if (result == DialogResult.OK)
                vaderDevice.SetColour(colorDialog.Color);
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            vaderDevice.SetPosition(9);
        }

        private void middleButton_Click(object sender, EventArgs e)
        {
            vaderDevice.SetPosition(4);
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            vaderDevice.SetPosition(0);
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            vaderDevice.Test();
        }

        private void twitterButton_Click(object sender, EventArgs e)
        {
            GetNextTweet();
        }

        private void twitterToggleButton_Click(object sender, EventArgs e)
        {
            tweetTimer.Enabled = !tweetTimer.Enabled;
            twitterToggleButton.Text = tweetTimer.Enabled ? "Stop" : "Start";
        }

        private void tweetTimer_Tick(object sender, EventArgs e)
        {
            GetNextTweet();
        }

        private void GetNextTweet()
        {
            var result = tweetQueue.GetNextTweet();
            if (result != null)
            {

                string command = Utils.ParseTweet(result.text);
                if (command != null)
                {
                    userLabel.Text = "@" + result.user.screen_name;
                    tweetTextBox.Text = result.text;
                    commandLabel.Text = command;
                    vaderDevice.Send(command);
                }

            }

        }
    }
}
