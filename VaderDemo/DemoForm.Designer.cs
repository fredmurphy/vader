﻿namespace VaderDemo
{
    partial class DemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.redButton = new System.Windows.Forms.Button();
            this.greenButton = new System.Windows.Forms.Button();
            this.blueButton = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.selectButton = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.middleButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.colourGroupBox = new System.Windows.Forms.GroupBox();
            this.positionGroupBox = new System.Windows.Forms.GroupBox();
            this.testButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.commandLabel = new System.Windows.Forms.Label();
            this.userLabel = new System.Windows.Forms.Label();
            this.tweetTextBox = new System.Windows.Forms.TextBox();
            this.twitterToggleButton = new System.Windows.Forms.Button();
            this.tweetTimer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.serarchTextLabel = new System.Windows.Forms.Label();
            this.colourGroupBox.SuspendLayout();
            this.positionGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // redButton
            // 
            this.redButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.redButton.Location = new System.Drawing.Point(9, 38);
            this.redButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.redButton.Name = "redButton";
            this.redButton.Size = new System.Drawing.Size(188, 69);
            this.redButton.TabIndex = 0;
            this.redButton.Text = "Red";
            this.redButton.UseVisualStyleBackColor = true;
            this.redButton.Click += new System.EventHandler(this.redButton_Click);
            // 
            // greenButton
            // 
            this.greenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greenButton.Location = new System.Drawing.Point(9, 117);
            this.greenButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.greenButton.Name = "greenButton";
            this.greenButton.Size = new System.Drawing.Size(188, 69);
            this.greenButton.TabIndex = 1;
            this.greenButton.Text = "Green";
            this.greenButton.UseVisualStyleBackColor = true;
            this.greenButton.Click += new System.EventHandler(this.greenButton_Click);
            // 
            // blueButton
            // 
            this.blueButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blueButton.Location = new System.Drawing.Point(9, 195);
            this.blueButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.blueButton.Name = "blueButton";
            this.blueButton.Size = new System.Drawing.Size(188, 69);
            this.blueButton.TabIndex = 2;
            this.blueButton.Text = "Blue";
            this.blueButton.UseVisualStyleBackColor = true;
            this.blueButton.Click += new System.EventHandler(this.blueButton_Click);
            // 
            // selectButton
            // 
            this.selectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectButton.Location = new System.Drawing.Point(9, 274);
            this.selectButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(188, 69);
            this.selectButton.TabIndex = 3;
            this.selectButton.Text = "Select...";
            this.selectButton.UseVisualStyleBackColor = true;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // upButton
            // 
            this.upButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upButton.Location = new System.Drawing.Point(9, 40);
            this.upButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(188, 69);
            this.upButton.TabIndex = 4;
            this.upButton.Text = "Up";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // middleButton
            // 
            this.middleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.middleButton.Location = new System.Drawing.Point(9, 118);
            this.middleButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.middleButton.Name = "middleButton";
            this.middleButton.Size = new System.Drawing.Size(188, 69);
            this.middleButton.TabIndex = 5;
            this.middleButton.Text = "Middle";
            this.middleButton.UseVisualStyleBackColor = true;
            this.middleButton.Click += new System.EventHandler(this.middleButton_Click);
            // 
            // downButton
            // 
            this.downButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downButton.Location = new System.Drawing.Point(9, 197);
            this.downButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(188, 69);
            this.downButton.TabIndex = 6;
            this.downButton.Text = "Down";
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // colourGroupBox
            // 
            this.colourGroupBox.Controls.Add(this.redButton);
            this.colourGroupBox.Controls.Add(this.greenButton);
            this.colourGroupBox.Controls.Add(this.selectButton);
            this.colourGroupBox.Controls.Add(this.blueButton);
            this.colourGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colourGroupBox.Location = new System.Drawing.Point(18, 18);
            this.colourGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.colourGroupBox.Name = "colourGroupBox";
            this.colourGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.colourGroupBox.Size = new System.Drawing.Size(210, 354);
            this.colourGroupBox.TabIndex = 7;
            this.colourGroupBox.TabStop = false;
            this.colourGroupBox.Text = "Colour";
            // 
            // positionGroupBox
            // 
            this.positionGroupBox.Controls.Add(this.testButton);
            this.positionGroupBox.Controls.Add(this.upButton);
            this.positionGroupBox.Controls.Add(this.middleButton);
            this.positionGroupBox.Controls.Add(this.downButton);
            this.positionGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.positionGroupBox.Location = new System.Drawing.Point(262, 18);
            this.positionGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.positionGroupBox.Name = "positionGroupBox";
            this.positionGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.positionGroupBox.Size = new System.Drawing.Size(210, 354);
            this.positionGroupBox.TabIndex = 8;
            this.positionGroupBox.TabStop = false;
            this.positionGroupBox.Text = "Position";
            // 
            // testButton
            // 
            this.testButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testButton.Location = new System.Drawing.Point(9, 274);
            this.testButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(188, 69);
            this.testButton.TabIndex = 7;
            this.testButton.Text = "Test...";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.serarchTextLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.commandLabel);
            this.groupBox1.Controls.Add(this.userLabel);
            this.groupBox1.Controls.Add(this.tweetTextBox);
            this.groupBox1.Controls.Add(this.twitterToggleButton);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(522, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1017, 354);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Twitter";
            // 
            // commandLabel
            // 
            this.commandLabel.AutoSize = true;
            this.commandLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandLabel.Location = new System.Drawing.Point(10, 314);
            this.commandLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.commandLabel.Name = "commandLabel";
            this.commandLabel.Size = new System.Drawing.Size(133, 22);
            this.commandLabel.TabIndex = 5;
            this.commandLabel.Text = "COLOUR #F00";
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userLabel.Location = new System.Drawing.Point(8, 137);
            this.userLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(109, 37);
            this.userLabel.TabIndex = 4;
            this.userLabel.Text = "@user";
            // 
            // tweetTextBox
            // 
            this.tweetTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.tweetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tweetTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tweetTextBox.Location = new System.Drawing.Point(9, 179);
            this.tweetTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tweetTextBox.Multiline = true;
            this.tweetTextBox.Name = "tweetTextBox";
            this.tweetTextBox.Size = new System.Drawing.Size(704, 53);
            this.tweetTextBox.TabIndex = 3;
            this.tweetTextBox.Text = "#hackitmsft vader red";
            // 
            // twitterToggleButton
            // 
            this.twitterToggleButton.Location = new System.Drawing.Point(9, 43);
            this.twitterToggleButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.twitterToggleButton.Name = "twitterToggleButton";
            this.twitterToggleButton.Size = new System.Drawing.Size(112, 52);
            this.twitterToggleButton.TabIndex = 1;
            this.twitterToggleButton.Text = "Start";
            this.twitterToggleButton.UseVisualStyleBackColor = true;
            this.twitterToggleButton.Click += new System.EventHandler(this.twitterToggleButton_Click);
            // 
            // tweetTimer
            // 
            this.tweetTimer.Interval = 5000;
            this.tweetTimer.Tick += new System.EventHandler(this.tweetTimer_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(128, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 26);
            this.label1.TabIndex = 6;
            this.label1.Text = "Waiting for tweets containing";
            // 
            // serarchTextLabel
            // 
            this.serarchTextLabel.AutoSize = true;
            this.serarchTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serarchTextLabel.Location = new System.Drawing.Point(128, 66);
            this.serarchTextLabel.Name = "serarchTextLabel";
            this.serarchTextLabel.Size = new System.Drawing.Size(529, 26);
            this.serarchTextLabel.TabIndex = 7;
            this.serarchTextLabel.Text = "#hackitmsft vader   red / green / blue / up / down / etc.";
            // 
            // DemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1557, 388);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.positionGroupBox);
            this.Controls.Add(this.colourGroupBox);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "DemoForm";
            this.Text = "Vader notification demo";
            this.colourGroupBox.ResumeLayout(false);
            this.positionGroupBox.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button redButton;
        private System.Windows.Forms.Button greenButton;
        private System.Windows.Forms.Button blueButton;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button selectButton;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button middleButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.GroupBox colourGroupBox;
        private System.Windows.Forms.GroupBox positionGroupBox;
        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tweetTextBox;
        private System.Windows.Forms.Button twitterToggleButton;
        private System.Windows.Forms.Timer tweetTimer;
        private System.Windows.Forms.Label commandLabel;
        private System.Windows.Forms.Label userLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label serarchTextLabel;
    }
}

