﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaderDemo.Twitter
{
    public class SearchResult
    {
        public Status[] statuses { get; set; }
        public Search_Metadata search_metadata { get; set; }
    }

    public class Search_Metadata
    {
        public float completed_in { get; set; }
        public long max_id { get; set; }
        public string max_id_str { get; set; }
        //public string next_results { get; set; }
        public string query { get; set; }
        public string refresh_url { get; set; }
        //public int count { get; set; }
        public long since_id { get; set; }
        public string since_id_str { get; set; }
    }

    public class Status
    {
        public long id { get; set; }
        public string id_str { get; set; }
        public string text { get; set; }
        public User user { get; set; }
    }

    public class User
    {
        public long id { get; set; }
        public string id_str { get; set; }
        public string name { get; set; }
        public string screen_name { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public string profile_image_url { get; set; }
    }
 
}

