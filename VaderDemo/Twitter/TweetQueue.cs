﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaderDemo.Twitter
{
    public interface ITweetQueue
    {
        Status GetNextTweet();
    }

    public class TweetQueue : ITweetQueue
    {
        private ITweetClient tweetClient;
        private string searchString;
        string lastTweetReturned;
        Queue<Status> tweetQueue = new Queue<Status>();

        public TweetQueue(ITweetClient TweetClient, string SearchString)
        {
            tweetClient = TweetClient;
            searchString = SearchString;
            lastTweetReturned = null;
        }

        public Status GetNextTweet()
        {
            GetTweetsIfEmpty();
                        
            return tweetQueue.Count > 0 ? tweetQueue.Dequeue() : null;
        }

        private void GetTweetsIfEmpty()
        {
            if (tweetQueue.Count == 0)
            {
                var searchResults = tweetClient.GetTweets(searchString, lastTweetReturned);

                if (searchResults == null)
                    return;
                if (searchResults.statuses.Length == 0)
                    return;

                long xid = searchResults.statuses[0].id;
                for (int x = 0; x < searchResults.statuses.Length; x++)
                {
                    if (searchResults.statuses[x].id > xid)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                    xid = searchResults.statuses[x].id;
                }

                for (int i = searchResults.statuses.Length-1; i >= 0; i--)
                    tweetQueue.Enqueue(searchResults.statuses[i]);
                lastTweetReturned = searchResults.search_metadata.max_id_str;

             
            }
        }
}
}
