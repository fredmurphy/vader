﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace VaderDemo.Twitter
{
    public interface ITweetClient
    {
        string GetAccessToken();
        SearchResult GetTweets(string searchString, string sinceId = null, string accessToken = null);
    }

    public class TweetClient : ITweetClient
    {
        private string oAuthConsumerSecret;
        private string oAuthConsumerKey;

        public TweetClient(string OAuthConsumerKey, string OAuthConsumerSecret)
        {
            oAuthConsumerKey = OAuthConsumerKey;
            oAuthConsumerSecret = OAuthConsumerSecret;
        }

        public SearchResult GetTweets(string searchString, string sinceId = null, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = GetAccessToken();
            }

            string url = "https://api.twitter.com/1.1/search/tweets.json?";
            if (sinceId != null)
                url += "since_id=" + sinceId + "&";
            url += string.Format("q={0}", HttpUtility.UrlEncode(searchString));

            var searchRequest = new HttpRequestMessage(HttpMethod.Get, url);
            searchRequest.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = httpClient.SendAsync(searchRequest).Result;
            var serializer = new JavaScriptSerializer();
            string response = responseUserTimeLine.Content.ReadAsStringAsync().Result;

            return serializer.Deserialize<SearchResult>(response);

        }

        public string GetAccessToken()
        {
            var httpClient = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.twitter.com/oauth2/token ");
            var customerInfo = Convert.ToBase64String(new UTF8Encoding().GetBytes(oAuthConsumerKey + ":" + oAuthConsumerSecret));
            request.Headers.Add("Authorization", "Basic " + customerInfo);
            request.Content = new StringContent("grant_type=client_credentials", Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = httpClient.SendAsync(request).Result;

            string json = response.Content.ReadAsStringAsync().Result;
            var serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(json);
            return item["access_token"];
        }
    }
}