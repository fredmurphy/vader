﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaderDemo
{
    public class Utils
    {
        public static string ColorToHex3(Color color)
        {
            return "#" + (color.R >> 4).ToString("X1") + (color.G >> 4).ToString("X1") + (color.B >> 4).ToString("X1");
        }

        public static string ParseTweet(string tweet)
        {
            string lower = tweet.ToLower();

            if (!lower.Contains("vader"))
                return null;

            if (lower.Contains("up"))
                return "POSITION 9";
            if (lower.Contains("middle"))
                return "POSITION 4";
            if (lower.Contains("down"))
                return "POSITION 0";

            if (lower.Contains("red"))
                return "COLOUR #F00";
            if (lower.Contains("green"))
                return "COLOUR #0F0";
            if (lower.Contains("blue"))
                return "COLOUR #00F";
            if (lower.Contains("yellow"))
                return "COLOUR #FF0";
            if (lower.Contains("purple"))
                return "COLOUR #F0F";
            if (lower.Contains("orange"))
                return "COLOUR #F80";
            if (lower.Contains("off"))
                return "COLOUR #000";
            if (lower.Contains("black"))
                return "COLOUR #000";
            if (lower.Contains("white"))
                return "COLOUR #FFF";

            if (lower.Contains("test"))
                return "TEST";

            return null;
        }

        public static Color? ParseTweetForColour(string tweet)
        {
            string lower = tweet.ToLower();

            if (lower.Contains("red"))
                return Color.Red;
            if (lower.Contains("green"))
                return Color.FromArgb(0, 255, 0);
            if (lower.Contains("blue"))
                return Color.Blue;
            if (lower.Contains("yellow"))
                return Color.Yellow;
            if (lower.Contains("purple"))
                return Color.FromArgb(255, 0, 255);
            if (lower.Contains("orange"))
                return Color.Orange;
            if (lower.Contains("off"))
                return Color.Black;
            if (lower.Contains("black"))
                return Color.Black;
            if (lower.Contains("white"))
                return Color.White;

            return null;
        }
    }
}
