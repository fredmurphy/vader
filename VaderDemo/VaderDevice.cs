﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaderDemo
{
    public interface IVaderDevice
    {
        void SetColour(Color lightsaberColour);
        void SetPosition(int position);
        void Test();
        void Send(string command);
    }

    public class VaderDevice : IVaderDevice
    {
        private SerialPort serialPort;

        public VaderDevice(string serialPortName)
        {
            serialPort = new SerialPort(serialPortName);
            serialPort.Open();
        }

        public void SetColour(Color lightsaberColour)
        {
            serialPort.WriteLine("COLOUR " + Utils.ColorToHex3(lightsaberColour));
        }

        public void SetPosition(int position)
        {
            if (position < 0 || position > 9)
                throw new ArgumentOutOfRangeException("Position should be from 0 to 9");

            serialPort.WriteLine("POSITION " + position.ToString());
        }

        public void Test()
        {
            serialPort.WriteLine("TEST");
        }

        /// <summary>
        /// Sorry - quick hack to get it working in time for demo
        /// </summary>
        /// <param name="command"></param>
        public void Send(string command)
        {
            serialPort.WriteLine(command);
        }

        private string ColorToHex3(Color color)
        {
            return "#" + (color.R >> 4).ToString("X1") + (color.G >> 4).ToString("X1") + (color.B >> 4).ToString("X1");
        }
    }

    public class DebugVaderDevice : IVaderDevice
    {
        public void SetColour(Color lightsaberColour)
        {
            Debug.WriteLine("[VADER] COLOUR " + Utils.ColorToHex3(lightsaberColour));
        }

        public void SetPosition(int position)
        {
            Debug.WriteLine("[VADER] POSITION " + position.ToString());
        }

        public void Test()
        {
            Debug.WriteLine("[VADER] TEST");
        }

        public void Send(string command)
        {
            Debug.WriteLine("[VADER] " + command);
        }
    }
}
