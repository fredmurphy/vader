﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VaderDemo.Twitter;

namespace VaderDemoTest.Twitter
{
    [TestClass]
    public class TweetQueueTest
    {
        private const string SearchString = "#vader";

        SearchResult twoTweets = new SearchResult
        {
            search_metadata = new Search_Metadata { max_id = 2, max_id_str = "2" },
            statuses = new Status[] { new Status { id = 2, text = "Second" }, new Status { id = 1, text = "First" } }
        };

        SearchResult thirdTweet = new SearchResult
        {
            search_metadata = new Search_Metadata { max_id = 3, max_id_str = "3" },
            statuses = new Status[] { new Status { id = 3, text = "Third" }}
        };

        SearchResult noTweets = new SearchResult
        {
            search_metadata = new Search_Metadata { max_id = 0, max_id_str = "0" },
            statuses = new Status[0]
        };

        [TestMethod]
        public void GetNextTweetShouldReturnOldestTweetAvailable()
        {

            var mockClient = GivenAMockTweetClient();

            var target = new TweetQueue(mockClient.Object, SearchString);

            var result = target.GetNextTweet();

            Assert.AreEqual(1, result.id);
            Assert.AreEqual("First", result.text);
        }

        [TestMethod]
        public void WhenCalledAgainGetNextTweetShouldReturnNextTweetAvailable()
        {

            var mockClient = GivenAMockTweetClient();

            var target = new TweetQueue(mockClient.Object, SearchString);

            target.GetNextTweet();
            var result = target.GetNextTweet();

            Assert.AreEqual(2, result.id);
            Assert.AreEqual("Second", result.text);
        }

        [TestMethod]
        public void WhenNothingInFirstBatchGetNextTweetShouldReturnNewSet()
        {

            var mockClient = GivenAMockTweetClient();

            var target = new TweetQueue(mockClient.Object, SearchString);

            target.GetNextTweet();
            target.GetNextTweet();
            var result = target.GetNextTweet();

            Assert.AreEqual(3, result.id);
            Assert.AreEqual("Third", result.text);
        }

        [TestMethod]
        public void WhenNothingRemainingGetNextTweetShouldReturnNull()
        {

            var mockClient = GivenAMockTweetClient();

            var target = new TweetQueue(mockClient.Object, SearchString);

            target.GetNextTweet();
            target.GetNextTweet();
            target.GetNextTweet();
            var result = target.GetNextTweet();

            Assert.IsNull(result);
        }


        private Mock<ITweetClient> GivenAMockTweetClient()
        {
            var mockClient = new Mock<ITweetClient>();
            mockClient.Setup(c => c.GetTweets(SearchString, null, null)).Returns(twoTweets);
            mockClient.Setup(c => c.GetTweets(SearchString, "2", null)).Returns(thirdTweet);
            mockClient.Setup(c => c.GetTweets(SearchString, "3", null)).Returns(noTweets);
            return mockClient;
        }
    }
}
